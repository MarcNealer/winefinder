from django.contrib import admin
from models import *

admin.site.register(Auction)
admin.site.register(Product)
admin.site.register(TastingNote)
admin.site.register(Lot)
admin.site.register(connectedNotes)