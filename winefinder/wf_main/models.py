from django.db import models
from django.contrib.auth.models import User
import numpy
import math
# Create your models here.



class Auction(models.Model):
    title = models.TextField(blank=True, null=True, default='')
    auction_close = models.CharField(max_length=100,blank=True)
    auction_id = models.IntegerField(null=True, default=0)


    def __unicode__(self):
        return self.title

class Lot(models.Model):
    auction = models.ForeignKey('Auction')
    stock_id = models.IntegerField(null=True)
    quantity = models.IntegerField(null=True, default=0)
    autobid_price = models.IntegerField(null=True, default=0)
    bid_estimated_high = models.IntegerField(null=True, default=0)
    bid_estimated_low = models.IntegerField(null=True, default=0)
    lot_number = models.IntegerField()
    notes = models.TextField(blank=True, null=True, default='')
    products = models.ManyToManyField('Product')

    def __unicode__(self):
        return "%s Lot Number %d" % (self.auction.title, self.lot_number)


class Product(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True, default='')
    bottle_size = models.CharField(max_length=255, blank=True, null=True, default=None)
    bottle_size_id = models.IntegerField(null=True, default=None)
    basket_id = models.IntegerField(null=True, default='')
    buy_now = models.TextField(blank=True, null=True, default='')
    classification = models.CharField(max_length=255, blank=True, null=True, default='')
    country = models.CharField(max_length=255, blank=True, null=True, default='')
    image_path = models.CharField(max_length=255, blank=True, null=True, default='')
    is_auction = models.CharField(max_length=255, blank=True, null=True, default='')
    is_buynow = models.BooleanField(default=True)
    is_prerelease = models.BooleanField(default=True)
    is_registered = models.BooleanField(default=True)
    notes = models.TextField(blank=True, null=True, default='')
    product_url = models.CharField(max_length=255, blank=True, null=True, default='')
    product_id = models.IntegerField(null=True, default=None)
    basket_id = models.IntegerField(null=True, default=None)
    region = models.CharField(max_length=255, blank=True, null=True, default='')
    subtitle = models.CharField(max_length=255, blank=True, null=True, default='')
    variety = models.CharField(max_length=255, blank=True, null=True, default='')
    vintage = models.CharField(max_length=255, blank=True, null=True, default='')
    abstract = models.TextField(blank=True, null=True, default='')
    source = models.CharField(max_length=255, blank=True, null=True, default='')

    def __unicode__(self):
        return "%s - %s (%s)" % (self.title,self.subtitle,self.vintage)
    def rating(self):
        rt= numpy.mean([x.note.safe_rating() for x in connectedNotes.objects.filter(product=self)])
        if rt == rt:
            return rt
        else:
            return 0.0


    def winery_rating(self):
        recs=wineryRating.objects.filter(winery__icontains=self.title)
        if recs.exists():
            return recs[0].rating
        else:
            return 'None'

    def vintage_rating(self):
        recs=vintageRating.objects.filter(winery__icontains=self.title, year=self.vintage)
        if recs.exists():
            return recs[0].rating
        else:
            return 'None'


    def price_low(self):
        return Lot.objects.filter(products=self)[0].bid_estimated_low
    def price_low(self):
        return Lot.objects.filter(products=self)[0].bid_estimated_high

class TastingNote(models.Model):
    wine_name = models.CharField(max_length=255, blank=True, null=True, default='')
    winery = models.CharField(max_length=255, blank=True, null=True, default='')
    region = models.CharField(max_length=255, blank=True, null=True, default='')
    variety = models.CharField(max_length=255, blank=True, null=True, default='')
    vintage = models.CharField(max_length=255, blank=True, null=True, default='')
    rating = models.CharField(max_length=100,blank=True,null=True)
    drink_by = models.CharField(max_length=100,blank=True,null=True)
    price = models.CharField(max_length=8, blank=True, null=True)
    date_tasted = models.DateTimeField(blank=True, null=True)
    alcohol = models.CharField(max_length=20, blank=True, null=True, default='')
    tasting_url = models.CharField(max_length=255, blank=True, null=True, default='')
    is_special = models.BooleanField(default=False)
    notes = models.TextField(blank=True)

    def __unicode__(self):
        return '%s-%s-%s' %(self.wine_name, self.alcohol, self.drink_by)
    def safe_rating(self):
        try:
            return int(self.rating)
        except:
            return 0

class connectedNotes(models.Model):
    product = models.ForeignKey('Product')
    note = models.ForeignKey('TastingNote')

    def __unicode__(self):
        return "%s - %s" % (self.product.title,self.note.wine_name)

class vintageRating(models.Model):
    winery  = models.CharField(max_length=200)
    year =  models.CharField(max_length=10)
    rating = models.CharField(max_length=10,blank=True)

    def __unicode__(self):
        return "%s-%s" % (self.winery,self.year)

class wineryRating(models.Model):
    winery = models.CharField(max_length=200)
    rating = models.CharField(max_length=10,blank=True)


