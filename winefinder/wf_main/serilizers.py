__author__ = 'marc'
from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from models import *
from django.core.exceptions import *


class UserModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name','last_name','username','email','last_login','is_active')

class AuctionsModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Auction

class LotModelSerializer(serializers.ModelSerializer):
    products = serializers.StringRelatedField(many=True,read_only=True)
    class Meta:
        model = Lot


class ProductModelSerializer(serializers.ModelSerializer):
    rating=serializers.FloatField(read_only=True)
    winery_rating=serializers.CharField(read_only=True)
    vintage_rating=serializers.CharField(read_only=True)
    price_low = serializers.FloatField(read_only=True)
    price_high= serializers.FloatField(read_only=True)
    class Meta:
        model = Product

class NotesModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = TastingNote