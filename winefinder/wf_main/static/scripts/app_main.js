var winefinder = angular.module('winefinder',[ 'ngAnimate','ngRoute','ngSanitize' ]);

winefinder.config(function ($routeProvider, $locationProvider) {
	$routeProvider
	.when('/', {
		templateUrl: '/static/snippets/auctions.html',
		controller: 'auctions'
	})
	.when('/products/:auctionID', {
		templateUrl: '/static/snippets/products.html',
		controller: 'products'
	})
	.when('/products/product/:productID', {
		templateUrl: '/static/snippets/details.html',
		controller: 'details'
	})
	.otherwise({
		redirectTo: '/'
	});
});

winefinder.controller( 'appMain' , function( $scope, $timeout, $http ){
	$scope.data = {};
	$scope.data.products = [];

	function getUser(){
		$http.get('/api/login/')
		.success( function(data){
			$scope.user = data;
			$scope.data.loggedIn = true;
			getAuctions();
		})
		.error( function(data, status, headers, config){
			if( status == 403 ){ $scope.data.loggedIn = false; }
		});
	}
	getUser();

	$scope.doLogin = function(){
		$scope.loggingIn = true;

		$http.post('/api/login/', {username: $scope.username,password:$scope.password}, { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data){
			$scope.data.user = data;
			$scope.data.loggedIn = true;
			$scope.loggingIn = false;
			getUser();
		})
		.error( function(data, status, headers, config){
			if( status == 401 ){ $scope.formMessage = 'Bad username/password.'; $scope.loggingIn = false; }
		});
	}

	$scope.doLogout = function(){
		$http.delete('/api/login/', { headers: { 'X-CSRFToken': getCookie('csrftoken') } } )
		.success( function(data){
			window.location.assign('/');
		})
		.error( function(data, status, headers, config){

		});
	}

	function getAuctions(){
		$http.get('/api/auctions/')
		.success( function(data){
			$scope.data.auctions = data;
		})
		.error( function(data, status, headers, config){
			if( status == 403 ){ }
		});
	}
});

function getCookie(cname){ var name = cname + "="; var ca = document.cookie.split(';'); for(var i=0; i<ca.length; i++) { var c = ca[i]; while (c.charAt(0)==' ') c = c.substring(1); if (c.indexOf(name) == 0) return c.substring(name.length,c.length); } return ""; }
