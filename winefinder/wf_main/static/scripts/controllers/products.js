winefinder.controller( 'products' , function( $scope, $timeout, $http, $routeParams ){
	$("body").animate({ scrollTop: 0 }, "fast");

	$scope.sortKey = 'title';
	$scope.reverse = false;
	if( !$scope.data.loggedIn ){ window.location.assign( '/' ); }


	function getProducts(){
		$scope.data.fetchingData = true;
		$http.get('/api/products/' + $routeParams.auctionID + '/' )
		.success( function(data){
			$scope.data.products = data;
			$scope.data.fetchingData = false;
		})
		.error( function(data, status, headers, config){
			if( status == 403 ){ }
		});
	}
	if( typeof $scope.data.products == 'undefined' || !$scope.data.products.length ){
		getProducts();
	}

	$scope.product = function(productID){
		loc = '#/products/product/' + productID;
		window.location.assign( loc );
	}

	$scope.vintageFilter = function ( prod ) {
		if( $scope.data.vintageFiltLow && $scope.data.vintageFiltHigh ){
			return ( parseInt( prod.vintage ) >= $scope.data.vintageFiltLow && parseInt( prod.vintage ) <= $scope.data.vintageFiltHigh );
		}else if( $scope.data.vintageFiltLow ){
			return ( parseInt( prod.vintage ) >= $scope.data.vintageFiltLow );
		}else if( $scope.data.vintageFiltHigh ){
			return ( parseInt( prod.vintage ) <= $scope.data.vintageFiltHigh );
		}else{
			return prod;
		}
	}
	$scope.ratingFilter = function ( prod ) {
		if( $scope.data.ratingFiltLow && $scope.data.ratingFiltHigh ){
			return ( parseInt( prod.rating ) >= $scope.data.ratingFiltLow && parseInt( prod.rating ) <= $scope.data.ratingFiltHigh );
		}else if( $scope.data.ratingFiltLow ){
			return ( parseInt( prod.rating ) >= $scope.data.ratingFiltLow );
		}else if( $scope.data.ratingFiltHigh ){
			return ( parseInt( prod.rating ) <= $scope.data.ratingFiltHigh );
		}else{
			return prod;
		}
	}
	$scope.priceFilter = function ( prod ) {
		if( $scope.data.priceFiltLow && $scope.data.priceFiltHigh ){
			return ( parseInt( prod.price_low ) >= $scope.data.priceFiltLow && parseInt(prod.price_low) <= $scope.data.priceFiltHigh );
		}else if( $scope.data.priceFiltLow ){
			return ( parseInt( prod.price_low ) >= $scope.data.priceFiltLow );
		}else if( $scope.data.priceFiltHigh ){
			return ( parseInt( prod.price_low ) <= $scope.data.priceFiltHigh );
		}else{
			return prod;
		}
	}
	$scope.sort = function(keyname){
        $scope.data.sortKey = keyname;   //set the sortKey to the param passed
        $scope.data.reverse = !$scope.data.reverse; //if true make it false and vice versa
    }
});
