winefinder.controller( 'details' , function( $scope, $timeout, $http, $routeParams ){
	$("body").animate({ scrollTop: 0 }, "fast");

	function getDetails(){
		$http.get('/api/product/' + $routeParams.productID + '/' )
		.success( function(data){
			$scope.product = data;
			
			$http.get('/api/notes/' + data.id + '/' )
			.success( function(data){
				$scope.notes = data;
			})
			.error( function(data, status, headers, config){
				if( status == 403 ){ }
			});

		})
		.error( function(data, status, headers, config){
			if( status == 403 ){ }
		});
	}
	getDetails();

});
