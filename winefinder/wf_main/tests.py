from django.test import TestCase
from rest_framework.test import APIClient
from wf_main.models import *
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
# Create your tests here.
class BlogsTestCase(TestCase):
    fixtures = ['testdata']

    def setUp(self):
        self.client = APIClient()
        self.client.login(username='marc',password='test0001')
    def test_login(self):
        """
        login to the system
        pre-con: username and password sent. Both are valid
        post-condition: if userame and pwd ar valid, the user rec is returned
        :return:
        """
        c=APIClient()
        payload={'username':'marc','password':'test0001'}
        resp = c.post('/api/login/', payload)
        self.assertContains(resp,'"username":"marc"',status_code=202)
    def test_logout(self):
        """
        pre-condition: User logged in
        post condition: user logged out, 200 returned
        :return:
        """
        resp = self.client.delete('/api/login/')
        self.assertEqual(resp.status_code,204)
        pass
    def test_isLoggedIn(self):
        """
        tests to see if the current user is authenticated on the system
        post-condition: returns 200 if yes or 400 if no
        :return:
        """
        resp=self.client.get('/api/login/')
        self.assertContains(resp,'"username":"marc"')
        pass
    def test_listAutions(self):
        """
        list auctions, newest first
        pre-cond: user is authenticated
        post-con: returns a pageanated list of auctions
        :return:
        """
        resp =self.client.get('/api/auctions/')
        self.assertContains(resp,'"title":"Single Vendor Collection"')
    def test_listLots(self):
        """
        Lists lots in a given aution
        pre-con: user is authenticated, aAuction is a valid auction
        post-con: a list of lots in JSON is returned
        :return:
        """
        resp = self.client.get('/api/lot/1/')
        self.assertContains(resp,'"stock_id":10629057')
    def test_listProducts(self):
        """
        Lst all prodicts in a give auction
        :return:
        """
        resp  = self.client.get('/api/products/1/')
        print resp
    def test_listProductDetail(self):
        """
        returns a product record
        pre-con: product id is valid. User is authenticated
        :return:
        """
        resp =  self.client.get("/api/product/454/")
        self.assertContains(resp,'"title":"FOX CREEK WINES"')
    def test_listNotes(self):
        """
        for a given product ID, returns a list of notes
        pre-con: user is authenticated, Product ID is valid
        post-con: JSON formated list of Notes
        :return:
        """
        resp = self.client.get('/api/notes/454/')
        self.assertContains(resp,'"wine_name":"3 Drops Great Southern Cabernets"')