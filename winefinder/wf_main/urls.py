__author__ = 'marc'
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login/$', views.UserLogin.as_view()),
    url(r'^auctions/$',views.AuctionList.as_view()),
    url(r'^lot/(?P<pk>[0-9]+)/$', views.LotsList.as_view()),
    url(r'^product/(?P<pk>[0-9]+)/$', views.ProductDetail.as_view()),
    url(r'^products/(?P<pk>[0-9]+)/$', views.ProductList.as_view()),
    url(r'^notes/(?P<pk>[0-9]+)/$', views.noteList.as_view())

]
