from django.contrib.auth import authenticate, login, logout
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.permissions import AllowAny
from serilizers import *
from rest_framework import generics
from django.core.exceptions import *
from django.http import Http404
# Create your views here.

class UserLogin(APIView):
    permission_classes = (AllowAny,)
    def get(self,request,format=None):
        if request.user.is_authenticated():
            serializer = UserModelSerializer(request.user)
            return Response(serializer.data,status=status.HTTP_200_OK)
        else:
            return Response('not logged in',status=status.HTTP_403_FORBIDDEN)
    def post(self,request,format=None):
        if 'username' in request.data and 'password' in request.data:
            user = authenticate(username=request.data['username'],
                                password=request.data['password'])
            if user is not None:
                login(request, user)
                serializer = UserModelSerializer(user)
                return Response(serializer.data,status=status.HTTP_202_ACCEPTED)
            else:
                return Response('bad credentials',status=status.HTTP_401_UNAUTHORIZED)
        else:
            return  Response('bad form',status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,format=None):
        logout(request)
        return Response('logged out',status=status.HTTP_204_NO_CONTENT)

class AuctionList(generics.ListAPIView):
    serializer_class = AuctionsModelSerializer
    queryset =  Auction.objects.all()
    paginate_by = 100

class LotsList(APIView):
    def get_object(self,pk):
        try:
            return Lot.objects.filter(auction__id=pk)
        except:
            raise Http404
    def get(self,request,pk,format=None):
        serializer = LotModelSerializer(self.get_object(pk),many=True)
        return Response(serializer.data)

class ProductDetail(generics.RetrieveAPIView):
    serializer_class = ProductModelSerializer
    queryset = Product.objects.all()

class ProductList(APIView):
    def get_object(self,pk):
        try:
            return Product.objects.filter(id__in=Lot.objects.filter(auction__id=pk).values_list('products__id',flat=True))
        except:
            raise Http404
    def get(self,request,pk,format=None):
        serializer=ProductModelSerializer(self.get_object(pk),many=True)
        return Response(serializer.data)

class noteList(APIView):
    def get(self,request,pk,format=None):
        queryset=TastingNote.objects.filter(id__in=[x.id for x in connectedNotes.objects.filter(product__id=pk)])
        serializer= NotesModelSerializer(queryset,many=True)
        return Response(serializer.data)

