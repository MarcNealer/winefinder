import multiprocessing
from optparse import make_option
from wf_main.models import *
from django.core.management.base import BaseCommand
from django.db import connection
from multiprocessing import Pool
from time import sleep, time
from random import randint
import os
import datetime
import requests
from bs4 import BeautifulSoup
from dateutil import parser

HEADERS = {'Accept-Encoding': 'gzip,deflate,sdch', 'Accept':
		 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
'User-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36'
 }
s = requests.Session()
LOGIN_URL = 'http://www.winecompanion.com.au/signin'
r = s.get(LOGIN_URL)
cookies = r.cookies
PAYLOAD = {
		'main_0$content_0$txtUsername': 'sdvfoley@gmail.com',
		'main_0$content_0$txtPassword': 'langtons1234',
		'main_0$ctl00$ctl00$hidShowDialog': True,
		'main_0$ctl00$ctl06$tbxName': '',
		'main_0$ctl00$ctl06$tbxEmail': '',
		'main_0$ctl00$ctl06$ddlState': 'ACT',
		'__VIEWSTATE': '',
		'__EVENTTARGET': 'main_0$content_0$btnLogin',
		'__VIEWSTATEGENERATOR': '325FA5A1',
		'ctl00$ctl01$txtUsername': '',
		'ctl00$ctl01$txtPassword': '',
		'ctl00$q': '',
		'ctl00$qhidden': '',
		'ctl00$ddlSearchType': ''
		}

def create_new_tasting(tasting_url):
    s.post(LOGIN_URL, data=PAYLOAD, headers=HEADERS, cookies=cookies)
    data = s.get(tasting_url)
    html = BeautifulSoup(data.text)
    tasting_section = html.find('div', 'tasting-section')
    rating = tasting_section.find('dl', 'keyvalues').find_all('dd')[0].text.strip()
    drink_by = tasting_section.find('dl', 'keyvalues').find_all('dd')[1].text.strip()
    price = tasting_section.find('dl', 'keyvalues').find_all('dd')[2].text.strip()
    date_tasted =  parser.parse(tasting_section.find('dl', 'keyvalues').find_all('dd')[3].text.strip())
    alcohol = tasting_section.find('dl', 'keyvalues').find_all('dd')[4].text.strip()
    notes = unicode(html.find('div', 'tasting-info'))

    wine_name = html.find('h1','section-title').text.strip()
    winery = html.find('dl', 'keyvalues').find_all('dd')[0].text.strip()
    region = html.find('dl', 'keyvalues').find_all('dd')[1].text.strip()
    variety = html.find('dl', 'keyvalues').find_all('dd')[2].text.strip()
    vintage = html.find('dl', 'keyvalues').find_all('dd')[3].text.strip()

    tasting_note = TastingNote(tasting_url=tasting_url)
    if html.find('h1').find_all('a', 'special-symbol'):
        tasting_note.is_special = True
    tasting_note.rating = rating
    tasting_note.drink_by = drink_by
    tasting_note.price = price
    tasting_note.date_tasted = date_tasted
    tasting_note.alcohol = alcohol
    tasting_note.notes = notes

    tasting_note.wine_name = wine_name
    tasting_note.winery = winery
    tasting_note.region = region
    tasting_note.variety = variety
    tasting_note.vintage = vintage
    return tasting_note


class AsyncFactory:
    def __init__(self, func, cb_func):
        self.func = func
        self.cb_func = cb_func
        self.pool = Pool(processes=5)

    def call(self, *args, **kwargs):
        self.pool.apply_async(self.func, args, kwargs, self.cb_func)

    def wait(self):
        self.pool.close()
        self.pool.join()


def cb_func(data):
    try:
        total, page = data
        print 'Completed creating %d products for the page %d ' % (total, page)
    except Exception, e:
        print str(e)


def get_notes_urls(page):
    url = 'http://www.winecompanion.com.au/search?t=1&page=%d&ps=20&=newest-tasting-note' % page
    urls = []
    try:
        r = requests.get(url)
        index1 = r.text.find('<ul class="listing-items">')
        index2 = r.text.find('<div id="main_0_content_0_pagerbottom" class="pager pager-bot cf">')
        text = r.text[index1:index2]
        html = BeautifulSoup(text)
        urls = ['http://www.winecompanion.com.au' + link.find('a')['href'] for link in html.find('ul').find_all('li')]
    except Exception, e:
        print 'get_notes_urls', str(e)
    return urls

def scape(page):
    connection.close()
    try:
        notes = []
        sleep_duration = randint(1, 5)
        print "PID: %d \t Sleep: %d" % (os.getpid(), sleep_duration)
        print 'Start creating products for page %d' % page

        tasting_urls = []

        for url in get_notes_urls(page):
            print url
            try:
                # Create new notes
                if not TastingNote.objects.filter(tasting_url=url)  and url not in tasting_urls:
                    tasting_urls.append(url)
                    notes.append(create_new_tasting(url))

            except Exception, e:
                print 'Errors: %s' % str(e)
        if notes:
            TastingNote.objects.bulk_create(notes)
        print 'Created total %s feeds for page %d' % (str(len(notes)), page)
        print 'End creating products for page %d' % page
        sleep(sleep_duration)
        return (len(notes), page)
    except Exception, e:
        print str(e)
        return None


class Command(BaseCommand):
    args = '<keyword ...>'
    help = 'Command to work with langtons'
    option_list = BaseCommand.option_list + (
        make_option('-p',
                    '--page',
                    help='Get all urls with page.',
                    dest='page',
                    default=False),
        make_option(
            "-f",
            "--from",
            dest="from",
            help="From page"
        ),
    )

    def handle(self, *args, **options):
        page = 89
        f = 1
        login = s.post(LOGIN_URL, data=PAYLOAD, headers=HEADERS, cookies=cookies)

        if options['page']:
            page = int(options['page'])
        if options['from']:
            f = int(options['from'])
        start_time = datetime.datetime.now()
        currtime = time()
        self.stdout.write('Started the job at %s' % start_time)
        results = []
        async_scape = AsyncFactory(scape, cb_func)
        for p in range(f, page):
            results.append(async_scape.call(p))

        async_scape.wait()
        self.stdout.write('Total %d' % len(results))
        end_time = datetime.datetime.now()
        self.stdout.write('Completed %s cronjobs at %s' % (str(len(results)), end_time))
