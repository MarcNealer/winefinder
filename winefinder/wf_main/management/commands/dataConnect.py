__author__ = 'marc'
from django.core.management.base import BaseCommand
from wf_main.models import *


def matchrecs():
    tastings = TastingNote.objects.all()
    products =Product.objects.all()
    for note in tastings:
        matches = products.filter(title__icontains=note.winery,vintage__icontains=note.vintage)
        if matches.exists():
            for prods in matches:
                if not connectedNotes.objects.filter(product=prods,note=note).exists():
                    connectedNotes.objects.create(product=prods,note=note)


class Command(BaseCommand):
    def handle(self, *args, **options):
        matchrecs()
        self.stdout.write('Completed')

