__author__ = 'marc'
from django.core.management.base import BaseCommand
from wf_main.models import *
from openpyxl import load_workbook
from optparse import make_option

def vintagerecs(filename):
    #import vintages
    wb = load_workbook(filename)
    ws = wb.get_active_sheet()
    cols = ['d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
            'aa','ab','ac','ad','ae','af','ag','ah','ai','aj','ak','al','am',
            'an','ao','ap','aq','ar','as','at','au','av','aw','ax','ay','az',
            'ba','bb','bc','bd','be','bf','bg','bh','bi','bj','bk','bl','bm',
            'bn','bo','bp','bq','br','bs','bt','bu','bv','bw','bx','by','bz']
    data=[]
    for num in range(2,200):
        winery = ws['b%d' % num].value
        if winery:
            for years in cols:
                year=ws['%s1' % years].value
                if year:
                    rating=ws['%s%d' % (years, num)].value
                    data.append([winery,year,rating])
    newrecs=[]
    for items in data:
        if vintageRating.objects.filter(winery=items[0],year=items[1]).exists():
            rec=vintageRating.objects.get(winery=items[0],year=items[1])
            rec.rating=items[2]
        else:
            newrecs.append(vintageRating(winery=items[0],year=items[1],rating=items[2]))
    vintageRating.objects.bulk_create(newrecs)



# import windery ratings

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option(
            "-f",
            "--file",
            dest="filename",
            help="specify import file",
            metavar="FILE"
        ),
    )
    def handle(self, *args, **options):
        vintagerecs(options['filename'])
        self.stdout.write('Completed')
