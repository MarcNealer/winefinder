__author__ = 'marc'
from django.core.management.base import BaseCommand
from wf_main.models import *
from openpyxl import load_workbook
from optparse import make_option
import csv

def wineryrecs(filename):
    #import vintages
    newrecs=[]
    try:
        f=open(filename)
        reader = csv.DictReader(f)
        for items in reader:
            if wineryRating.objects.filter(winery=items['Winery']).exists():
                rec=wineryRating.objects.get(winery=items['Winery'])
                rec.rating=items['Website Rating']
            else:
                newrecs.append(wineryRating(winery=items['Winery'],rating=items['Website Rating']))
        wineryRating.objects.bulk_create(newrecs)
    except:
        pass

# import windery ratings

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option(
            "-f",
            "--file",
            dest="filename",
            help="specify import file",
            metavar="FILE"
        ),
    )
    def handle(self, *args, **options):
        wineryrecs(options['filename'])
        self.stdout.write('Completed')

