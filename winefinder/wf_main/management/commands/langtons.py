import multiprocessing
from optparse import make_option
import requests
from django.core.management.base import BaseCommand
from wf_main.models import *
from multiprocessing.pool import Pool
from django.core.management.base import BaseCommand
from django.db import connection
from multiprocessing import Pool
from time import sleep, time
from random import randint
import os
import json
import datetime
from dateutil import parser
from bs4 import BeautifulSoup

class AsyncFactory:
    def __init__(self, func, cb_func):
        self.func = func
        self.cb_func = cb_func
        self.pool = Pool(processes=2)

    def call(self, *args, **kwargs):
        self.pool.apply_async(self.func, args, kwargs, self.cb_func)

    def wait(self):
        self.pool.close()
        self.pool.join()


def cb_func(data):
    try:
        total, page = data
        print 'Completed creating %d products for the page %d ' % (total, page)
    except Exception, e:
        print str(e)


def get_langtons_data(page):
    url = 'https://www.langtons.com.au/api/productlisting/'
    data = {
        "P": page,
        "SortASC": True,
        "SortTerm": "LotNo",
        "Terms": None,
        "IncludeAuction":True

    }

    r = requests.get(url, params=data)
    jsondata = json.loads(r.text)
    products = jsondata.get('Products')
    if products:
        return products
    return []

def get_langtons_desc(url):
    r = requests.get(url)
    html = BeautifulSoup(r.text)
    try:
        desc = html.find('div', 'product-classification-and-notes').findNextSiblings('p')[0].text
        return desc
    except:
        return ''

def scape(page):
    connection.close()
    try:
        products = []
        sleep_duration = randint(1, 10)
        print "PID: %d \t Sleep: %d" % (os.getpid(), sleep_duration)
        print 'Start creating products for page %d' % page

        product_ids = []
        for product in get_langtons_data(page):
            try:
                # Create new products
                if not Product.objects.filter(product_id=int(product['ProductId'])).exists():
                    # print 'ProductId %s' % str(product['ProductId'])
                    # print product_ids
                    desc = product['Abstract']
                    if desc != '':
                        desc = get_langtons_desc('https://www.langtons.com.au' + product['ProductDetailUrl'])
                    newProduct=Product.objects.create(title=product['Title'],
                                                      product_id=product['ProductId'],
                                                      bottle_size=product['BottleSize'],
                                                      bottle_size_id=int(product['BottleSizeId']),
                                                      buy_now=product['BuyNow'],
                                                      classification=product['Classification'],
                                                      country=product['Country'],
                                                      image_path=product['ImagePath'],
                                                      is_auction=product['IsAuction'],
                                                      is_buynow=product['IsBuyNow'],
                                                      is_prerelease=product['IsPrerelease'],
                                                      is_registered=product['IsRegisteredForGST'],
                                                      notes=product['Notes'],
                                                      product_url=product['ProductDetailUrl'],
                                                      region=product['Region'],
                                                      subtitle=product['Subtitle'],
                                                      variety=product['Variety'],
                                                      vintage=product['Vintage'],
                                                      abstract=desc,
                                                      basket_id=product['BasketId'],
                                                      source='langtons')
                else:
                    newProduct=Product.objects.get(product_id=int(product['ProductId']))

            except Exception, e:
                print 'Errors: %s' % str(e)

            #Create new auctions
            try:
                if product['Auctions']:
                    for auction in product['Auctions']:
                        if not Auction.objects.filter(auction_id=int(auction['AuctionId'])).exists():
                            newAuction=Auction.objects.create(auction_close=auction['AuctionCloseString'],
                                                              auction_id=auction['AuctionId'],
                                                              title=auction['Title'])
                        else:
                            newAuction = Auction.objects.get(auction_id=int(auction['AuctionId']))
                        if not Lot.objects.filter(auction=newAuction,lot_number=auction['LotNumber']).exists():
                            newLot=Lot.objects.create(autobid_price=auction['AutoBidPrice'],
                                                      bid_estimated_high=auction['BidEstimatedHigh'],
                                                      bid_estimated_low=auction['BidEstimatedLow'],
                                                      lot_number=auction['LotNumber'],
                                                      notes=auction['Notes'],
                                                      quantity=auction['Quantity'],
                                                      stock_id=auction['StockId'],
                                                      auction=newAuction)
                        else:
                            newLot=Lot.objects.get(auction=newAuction,lot_number=auction['LotNumber'])
                        newLot.products.add(newProduct)
                        newLot.save()

            except Exception, e:
                print str(e)
        sleep(sleep_duration)
        return (len(products), page)
    except Exception, e:
        print str(e)
        return None


class Command(BaseCommand):
    args = '<keyword ...>'
    help = 'Command to work with langtons'
    option_list = BaseCommand.option_list + (
        make_option('-p',
                    '--page',
                    help='Get all urls with page.',
                    dest='page',
                    default=False),
        make_option(
            "-f",
            "--file",
            dest="filename",
            help="specify import file",
            metavar="FILE"
        ),
    )

    def handle(self, *args, **options):
        page = 89
        if options['page']:
            page = int(options['page'])
        start_time = datetime.datetime.now()
        currtime = time()
        self.stdout.write('Started the job at %s' % start_time)
        results = []
        async_scape = AsyncFactory(scape, cb_func)
        for page in range(1, page):
            results.append(async_scape.call(page))

        async_scape.wait()
        self.stdout.write('Total %d' % len(results))
        end_time = datetime.datetime.now()
        self.stdout.write('Completed %s cronjobs at %s' % (str(len(results)), end_time))
